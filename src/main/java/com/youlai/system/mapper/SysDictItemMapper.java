package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.pojo.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}




