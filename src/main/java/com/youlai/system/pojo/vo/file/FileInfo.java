package com.youlai.system.pojo.vo.file;

import lombok.Data;

@Data
public class FileInfo {

    private String name;

    private String url;

}
